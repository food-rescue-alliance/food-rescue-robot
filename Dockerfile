FROM ruby:2.3.7
RUN mkdir /app
WORKDIR /app
ENV PATH=/usr/local/bundle/bin:/usr/local/bundle/gems/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
ENV BUNDLE_DEPLOYMENT=1
ENV BUNDLE_PATH=vendor/bundle
ENV BUNDLE_APP_CONFIG=/usr/local/bundle
ENV BUNDLE_SILENCE_ROOT_WARNING=1
ENV BUNDLE_BIN=vendor/bundle/bin
ENV BUNDLE_GLOBAL_PATH_APPENDS_RUBY_SCOPE=1
ENV PORT=3000

#alpine
#RUN apk update
#RUN apk add git postgresql postgresql-dev make gcc g++ musl-dev libxml2-dev libxslt-dev sqlite-dev python2

USER root

#debian
RUN sed -i s/deb.debian.org/archive.debian.org/g /etc/apt/sources.list
RUN sed -i 's|security.debian.org|archive.debian.org|g' /etc/apt/sources.list 
RUN sed -i '/stretch-updates/d' /etc/apt/sources.list
RUN apt update
RUN apt install git make gcc g++ libxml2-dev libxslt-dev sqlite python libv8-dev apt-transport-https vim -y

#postgres
RUN echo "deb [arch=amd64] http://apt-archive.postgresql.org/pub/repos/apt/ stretch-pgdg main" > /etc/apt/sources.list.d/pgdg.list
RUN curl -ksSL https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
RUN apt update
RUN apt install -y postgresql-10

#ruby
COPY ./ /app

#alpine specific gem installs on their own
RUN gem install bundler -v '~> 1.17.3'
#RUN gem install nokogiri -v '1.6.7.2' -- --use-system-libraries
#RUN gem install json -v '1.8.6'


#v8 things
#RUN gem install libv8 -v '3.16.14.16'
RUN bundle install

#oof, old libs
RUN sed -i 's|panic|warning|g' /app/vendor/bundle/ruby/2.3.0/gems/activerecord-3.2.22.5/lib/active_record/connection_adapters/postgresql_adapter.rb

# heroku friendly run command, with compile as required enabled
CMD bundle exec rails server thin -p $PORT -e production
